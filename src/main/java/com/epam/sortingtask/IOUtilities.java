package com.epam.sortingtask;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * Treats input and output
 * implements intterface IO
 * @author Svetlana_Zubkova
 */
class IOUtilities implements IO{
    private final Scanner scanner;
    private final PrintStream out;
    private final PrintStream err;

    IOUtilities(InputStream in, PrintStream out, PrintStream err) {
        this.scanner = new Scanner(in);
        this.out = new PrintStream(out);
        this.err = new PrintStream(err);
    }

    @Override
    public String arrayToSort() {
        System.out.println("Enter up to 10 integers:");
        return scanner.nextLine();
    }
}
