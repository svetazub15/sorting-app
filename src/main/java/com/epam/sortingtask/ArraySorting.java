package com.epam.sortingtask;

import java.util.Arrays;

/**
 * Treats input String
 * @author Svetlana_Zubkova
 */
class ArraySorting {
    /**
     * Sorts input String
     * gives result output String
     * @return String
     */
    String outputArray(String arrayToSort) {
        if (arrayToSort.length() > 10) {
            arrayToSort = arrayToSort.substring(0, 9);
        }
        char[] input = arrayToSort.toCharArray();
        Arrays.sort(input);
        return String.valueOf(input);
    }
}
