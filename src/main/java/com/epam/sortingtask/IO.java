package com.epam.sortingtask;

/**
 * Creates method for array input
 * @author Svetlana_Zubkova
 */
public interface IO {
    String arrayToSort();

}
