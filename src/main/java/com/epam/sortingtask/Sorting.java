package com.epam.sortingtask;

import java.util.NoSuchElementException;


/**
 * Makes sorting up to 10 intergers which entering in console
 * @author Svetlana_Zubkova
 */
class Sorting {
    private final IO io;

    public Sorting(IO io) {
        this.io = io;
    }

    public static void main(String[] args) {
        IO io = new IOUtilities(System.in, System.out, System.err);
        new Sorting(io).run();
    }

    private void run() {
        String stringToSort;
        try {
            stringToSort = io.arrayToSort();
            if (stringToSort.length() == 0) {
                System.err.println("Enter some number!");
            }
            String output = new ArraySorting().outputArray(stringToSort);
            System.out.println("Sorted array: " + output);
        } catch (NoSuchElementException e) {
            System.err.println(e.getMessage());
        }
    }


}
