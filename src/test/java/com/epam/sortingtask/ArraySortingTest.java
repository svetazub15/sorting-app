package com.epam.sortingtask;

import org.junit.Test;


import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests ArraySorting class
 * @author Svetlana_Zubkova
 */
public class ArraySortingTest {
    ArraySorting arraySorting = new ArraySorting();

    /**
     * Tests empty String as input
     */
    @Test
    public void testEmptyCase() {
        String sample = "";
        assertNotNull(arraySorting.outputArray(sample));
    }

    /**
     * Tests String with single integer as input
     */
    @Test
    public void testSingleElementArrayCase() {
        String expected = "3";
        String test = "3";
        assertEquals(expected, arraySorting.outputArray(test));
    }

    /**
     * Tests String with more that 10 integers as input
     */
    @Test
    public void testWithMoreThanTenIntergersCase() {
        String expected = "134567789";
        String test = "1379857463923";
        assertEquals(expected, arraySorting.outputArray(test));
    }

    /**
     * Tests String with 10 integers as input
     */
    @Test
    public void testWithTenIntergersCase() {
        String expected = "1234566778";
        String test = "1623456787";
        assertEquals(expected, arraySorting.outputArray(test));
    }
}
