package com.epam.sortingtask;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests ArraySorting class for different types of input (input with all similar integers, with repeated ones etc.)
 *
 * @author Svetlana_Zubkova
 */
public class ArraySortingParametrisedTest {
    ArraySorting arraySorting = new ArraySorting();

    @ParameterizedTest(name = "{index} => expected={0}, test={1}")
    @CsvSource({
            "56, 65",
            "2555789,  5985275",
            "333, 333",
            "035569, 350956"
    })
    public void sortWithDifferentInputs(String expected, String test) {
        assertEquals(expected, arraySorting.outputArray(test));
    }
}
